import sys
import time

from nlp.pos_tagger import tree, util, pcfg, config


def main():
    if len(sys.argv) < 3:
        print("""
    Usage: %s trainingFile outputGrammarFile
    """ % sys.argv[0])
        sys.exit(1)

    start_time = time.time()
    log = util.create_logger(__name__.upper(), config.LOGGING_LEVEL, 'W:\\Logs\\NLP')
    grammar = pcfg.PCFG(log)

    tree_count = 0
    log.info('Loading input training treebank: {}'.format(sys.argv[1]))
    with open(sys.argv[1], 'r') as f_in:
        for line in f_in:
            my_tree = tree.parse(line)
            tree_rules = my_tree.get_rules()
            for r in tree_rules:
                grammar.add_rule(r.start, r.end, has_leaf=r.has_leaf)
            tree_count += 1

    log.info('Computing probabilities')
    grammar.calculate_probabilities()
    log.info('Smoothing')
    grammar.do_smoothing()
    log.info('Saving result PCFG to {}'.format(sys.argv[2]))
    grammar.save_to_file(sys.argv[2])

    end_time = time.time()
    total_time = (end_time - start_time) / 60

    log.info('Total time: {} minute(s)'.format(total_time))
    log.info('Trees: {}, Rules: {}'.format(tree_count, grammar.rule_count()))


if __name__ == "__main__":
    main()
