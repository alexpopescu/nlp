import argparse
import multiprocessing
import sys
import time
from multiprocessing.pool import Pool

from nlp.pos_tagger import util, pcfg, config, cky


def parse_sentence(grammar, tokens, log_queue, log_name):
    log = util.create_mp_logger(log_queue, log_name, level=config.LOGGING_LEVEL)
    parsing_cky = cky.CKY(grammar, log)

    try:
        # fill the CKY table
        covering = parsing_cky.parse(tokens)

        # print covering non-terminals if desired
        if config.PRINT_COVERING:
            log.info("[covering]" + " ".join([c[3][0] for c in covering]))

        # print the Viterbi parse/derivation
        parse = parsing_cky.get_viterbi_parse()

        if config.REMOVE_FAKE_UNARIES_AND_DEBINARIZE:
            parse.clean_up()

        log.info(parse)
        return parse

    except Exception as e:
        log.error(str(e) + " (TOP (XXX " + ") (XXX ".join(tokens) + "))")


def parse_arguments():
    parser = argparse.ArgumentParser(description='Tags POS in given sentences using an already extracted PCFG')
    parser.add_argument('--grammar', '-g', dest='grammar', help='The file containing the grammar (PCFG)')
    parser.add_argument('--sentences', '-s', dest='sentences',
                        help='The file containing the sentences to be processed (one per line)')
    parser.add_argument('--output', '-o', dest='output', default=None,
                        help='The output file where to publish the results')
    parser.add_argument('--log-path', '-l', dest='log_path', default=None,
                        help='The path where to save the logs')
    parser.add_argument('--processes', '-p', dest='processes', default=8, type=int,
                        help='The number of processes to use')
    return parser.parse_args()


def main():
    args = parse_arguments()
    manager = multiprocessing.Manager()
    queue = manager.Queue(-1)
    log_listener = util.create_mp_listener(queue, __file__, args.log_path, config.LOGGING_LEVEL)

    start_time = time.time()

    log = util.create_mp_logger(queue, util.logify_name(__file__).upper(), level=config.LOGGING_LEVEL)
    grammar = pcfg.PCFG.from_file(args.grammar, log)
    sentences = []

    with open(args.sentences, "r") as f:
        for sentence in f:
            tokens = sentence.rstrip().split(" ")

            # skip sentences that are too long
            if len(tokens) > config.MAX_SENTENCE_LENGTH:
                log.info("Skipped a sentence (too long)")
                continue

            sentences.append(tokens)

    p = Pool(args.processes)
    result = p.starmap(parse_sentence, [(grammar, tokens, queue, 'S'+str(i)) for i, tokens in enumerate(sentences)])

    if args.output is not None:
        with open(args.output, 'w') as output_file:
            for parse in result:
                output_file.write(str(parse) + "\n")

    end_time = time.time()
    total_time = (end_time - start_time) / 60
    log.info("Total time: %f min", total_time)
    log_listener.stop()


if __name__ == "__main__":
    main()
