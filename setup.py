from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(name='nlp',
      version='1.0',
      description='A package containing algorithms and helper functions for Natural Language Processing',
      long_description=readme(),
      classifiers=[
          'Development Status :: 2 - Pre-Alpha',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.5',
          'Topic :: Scientific/Engineering :: Artificial Intelligence',
      ],
      keywords='funniest joke comedy flying circus',
      url='https://bitbucket.org/alexpopescu/nlp',
      author='Alex Popescu',
      author_email='mail@alexandrupopescu.com',
      license='MIT',
      packages=['nlp'],
      install_requires=[
      ],
      entry_points={
          'console_scripts': [
              'build-pcfg=entry_points.pos_tagger.create_pcfg:main',
              'pos-tagger=entry_points.pos_tagger.pos_tagger:main',
          ],
      },
      include_package_data=True,
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose'])
