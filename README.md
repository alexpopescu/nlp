# NLP Library

## POS Tagger
### Theory
A Probabilistic Context-Free Grammar (PCFG) is defined as a tuple $(N, \Sigma, R, S)$, with

- __N__: a set of non-terminals
- __T__: a set of terminal symbols (disjoint from _N_)
- __R__: a set of rules (productions) `A -> b`, where A is a non-terminal and b is a sequence of terminals
and non-terminals from the set `T U N`. Every rule has an associated probability `p ∈ (0,1]`
so that `∑P(A -> b)=1`
- __S__: a start symbol

PCFGs are considered to be the simplest probabilistic model for tree structures and contain information about 
frequent grammar builds that appear in a given corpus.
A probability `P(A -> b|A)` is the probability of the rule `A -> b` being produced, given `A`. 
Each rule will represent a subtree of the tree that represents a sentence.

In order to train a PCFG we use grammar learning (or induction). To determine the probability of a rule, we use the relative frequency estimate (RFE). This estimate is in this case equal to the maximum likelihood estimate (MLE). 
This results in the following formula:

![\widehat{P}(A\rightarrow\beta)=\frac{C(A\rightarrow\beta)}{\sum_\gamma{C(A\rightarrow\gamma)}} = \frac{C(A\rightarrow\beta)}{C(A)}](https://latex.codecogs.com/png.latex?\widehat{P}%28A\rightarrow\beta%29=\frac{C%28A\rightarrow\beta%29}{\sum_\gamma{C%28A\rightarrow\gamma%29}}&space;=&space;\frac{C%28A\rightarrow\beta%29}{C%28A%29})

where `C(rule)` is the count of the number of times that particular rule is used in the training corpus.
These probabilities are used for disambiguation by ranking the possible trees to establish which one is the most probable. That most probable tree will be the one that maximizes the probability

![P(T|S)=\frac{P(T,S)}{P(S)}](https://latex.codecogs.com/png.latex?P%28T|S%29=\frac{P%28T,S%29}{P%28S%29})

since the maximization is done for a sentence, `P(S)` will be the same for all trees, hence it is enough to maximize

![P(T,S)=P(T)P(S|T)=P(T)](https://latex.codecogs.com/png.latex?P%28T,S%29=P%28T%29P%28S|T%29=P%28T%29)

The probability of a _tree_ is calculated as the product of the probabilities of all subtrees (expressed as LHS<sup>i</sup> -> RHS<sup>i</sup>):

![P(T)=\prod_{i=1}^{n}{P(LHS^i\rightarrow{RHS^i})}](https://latex.codecogs.com/png.latex?P%28T%29=\prod_{i=1}^{n}{P%28LHS^i\rightarrow{RHS^i}%29})

The probability of a __sentence__ will be the sum of the probabilities of all possible trees for that sentence:

![P(S)=\sum_{T}{P(T)}](https://latex.codecogs.com/png.latex?P%28S%29=\sum_{T}{P%28T%29})

### Usage

A PCFG object can be created in two ways: 

#### Using a treebank to train a PCFG
In this case, each tree in the treebank will be processed and extracted rules will be added to the grammar.
At the end, the probabilities are calculated based on how many occurrences of each rule were observed

```python
from nlp.pos_tagger import pcfg, tree

grammar = pcfg.PCFG()
with open('/path/to/treebank', 'r') as f_in:
    for line in f_in:
        my_tree = tree.parse(line)
        tree_rules = my_tree.get_rules()
        for r in tree_rules:
            grammar.add_rule(r.start, r.end)

# Compute probabilities
grammar.calculate_probabilities()

# Optionally save grammar to file for reuse
grammar.save_to_file('/path/to/grammar_file')
```

#### Loading an existing PCFG from a file
After the grammar file was created, it can be reused without computing everything again

```python
from nlp.pos_tagger import pcfg, util

log = util.create_logger(__name__.upper())
grammar = pcfg.PCFG.from_file('/path/to/grammar_file', log)
```

***

## CKY

To find the most probable tree for a given sentence, we need an algorithm. 
There are many alternatives, such as Top-down, Bottom-up, Earley, and CKY. 
Here we employ the probabilistic version of CKY, which is widely used for this purpose. 

The __Cocke–Younger–Kasami__ algorithm (alternatively called CYK, or CKY) is a parsing algorithm for context-free grammars, named after its inventors, John Cocke, Daniel Younger and Tadao Kasami. 
It employs bottom-up parsing and dynamic programming.

### Theory

We will assume the PCFG used is in Chomsky Normal Form (CNF), although the algorithm can be modified to deal with grammars which are not in CNF.

CKY is a dynamic programming algorithm that iteratively fills out a \textbf{chart}. Let us assume that we are parsing a sentence with `n` words, for example "Stop that car!" where `n=3`. 
We can add indices to that sentence like this: 

`0` Stop `1` that `2` car! `3`	

The chart, the upper triangle of an `(n+1) x (n+1)` matrix, now looks like this:

|   |   1   |   2   |   3   |
|---|:-----:|:-----:|:-----:|
| 0 | [---] | [---] | [---] |
| 1 |       | [---] | [---] |
| 2 |       |       | [---] |

As the indices show, every phrase is surrounded by two indices. 
The __words__ are located in the lowest diagonal, that is `(0,1)`, `(1,2)` and `(2,3)`. 
The highest level phrase, the sentence, is always located at `(0,n)`, in this case `(0,3)`. 

CKY proceeds as follows:
It starts by populating all cells in the bottom diagonal with the non-terminals that directly generate the words. 
That is, in cell `(0,1)` all NTs are placed that generate the verb "_stop_", in `(1, 2)` all NTs that generate "_that_", and so on.

Now, CKY goes through the chart left-to-right bottom-to-top, excluding the cells that were just populated. 
At every cell, the algorithm tries to combine two non-terminals (say, `B` and `C`) that together (consecutively) cover the words under the cell, by finding a rule `A -> B,C` in the grammar. 
If such a rule exists, `A` is inserted into the cell. Note that the non-terminals `B` and `C` were added to the chart in a previous step of the algorithm; they are a solved sub-problem.

### Usage

#### Using a PCFG and CKY to parse sentences
```python
from nlp.pos_tagger import pcfg, util, cky

log = util.create_logger(__name__.upper())
grammar = pcfg.PCFG.from_file('/path/to/grammar_file', log)
parsing_cky = cky.CKY(grammar, log)

sentence = 'This is a simple sentence.'
tokens = sentence.rstrip().split(" ")

# fill the CKY table
covering = parsing_cky.parse(tokens)
```

## Viterbi
The above CYK algorithm is a recognizer that will only determine if a sentence is in the language defined by a given grammar.

### Theory
Given a span `<i,j>` and a non-terminal `A`, the Viterbi algorithm returns the highest probability derivation from `A` that covers the span `i` to `j`.
Viterbi is defined as follows:

![Viterbi(i, A, j) = \arg\max_{A \to B C, i < k < j} P(A \to B C) \times Viterbi(i, B, k) \times Viterbi(k, C, j)](https://latex.codecogs.com/png.latex?Viterbi%28i,A,j%29=\arg\max_{A%20\to%20B%20C,%20i<k<j}%20P%28A%20\to%20B%20C%29%20\times%20Viterbi%28i,B,k%29%20\times%20Viterbi%28k,C,j%29)

With a special border case for terminals:

![Viterbi(i, A, i+1) = \arg\max_{A \to w_i} P(A \to w_i)](https://latex.codecogs.com/png.latex?Viterbi%28i,A,i+1%29=\arg\max_{A%20\to%20w_i}%20P%28A%20\to%20w_i%29)

Note that this is very similar to the definition of __Inside__. 
The difference is that where Inside __sums__ over the probabilities of all possibilities to rewrite `A` (rules `A -> B C`), here we take the __maximum__ instead.

### Usage
After processing a sentence, the CKY parser will keep the backpointers needed to use Viterbi to retrieve the most probable derivation.

```python
from nlp.pos_tagger import pcfg, util, cky

log = util.create_logger(__name__.upper())
grammar = pcfg.PCFG.from_file('/path/to/grammar_file', log)
parsing_cky = cky.CKY(grammar, log)

sentence = 'This is a simple sentence.'
tokens = sentence.rstrip().split(" ")

# fill the CKY table
covering = parsing_cky.parse(tokens)

# use Viterbi to extract the sentence tree
parse = parsing_cky.get_viterbi_parse()

# optionally, the clean_up method can be used to remove fake unaries and revert binarizations
parse.clean_up()
```

## Unknown words and Smoothing
One of the problems with any statistical technique is that it will not deal very well with never before seen cases. 
For example, no matter how large our treebank is, if it doesn't contain all possible words it will still be incomplete. 
Any word that was not found at all will have a probability of 0.

### Theory
There are a few possible approaches to deal with this problem. 
Adding categories of unknown words to the grammar is an efficient one. 
The library defines some category of unknown words based on the morphology of the words. 
The used categories and their designated terminal symbols are:

| Category | Terminal |
|---|---|
| numbers | xxxunknown_number |
| capitalized words | xxxunknown_title |
| by suffix (-ed, -ing, -er, -est, -ize, -fy, -ly, -ble, -ness, -less, -ment, -ism, -ist, -al, -ish) | xxxunknown_&lt;SUFFIX&gt; |
| contains a hyphen | xxxunknown_hyphen |

The probabilities of each rule `LHS -> xxxunknown_&lt;X&gt;` were calculated normally by using the counts already encountered in the treebank, corresponding to each unknown category. 
Furthermore, all __numbers__ were aggregated into the `LHS -> xxxunknown_number` for each `LHS` and then removed from the PCFG as they wouldn't bring any useful information to the subsequent parsing.

Besides the categories mentioned above the grammar also includes a fallback category for completely unknown words that don't belong to any of the mentioned categories, called "xxxunknown_". 
The problem was that things we have never seen, as we menttioned, have a probability of zero. 
In order to calculate the probability of the unseen, there are a few discounting/smoothing algorithms that help, such as Laplace smoothing, Good-Turing smoothing, Witten-Bell discounting or Kneser-Ney smoothing.
This library implements the Good-Turing smoothing that uses the frequency of terminals that appear once as a re-estimate of the frequency of the ones that never occurred.
Basically, it will replace the count `c` for `Nc` (the items that were encountered `c` times) with a smoothed version:

![c^{*}=(c+1)\frac{N_{c+1}}{N_c}](https://latex.codecogs.com/png.latex?c^{*}=%28c+1%29\frac{N_{c+1}}{N_c})

The addition of all these features will ensure that we will always have a terminal in our PCFG for any word we are seeking to tag.

### Usage
The unknown words categories are computed automatically.

The Good-Turing Smoothing is implemented as an optional step when extracting the PCFG:
```python
from nlp.pos_tagger import pcfg, tree

grammar = pcfg.PCFG()
with open('/path/to/treebank', 'r') as f_in:
    for line in f_in:
        my_tree = tree.parse(line)
        tree_rules = my_tree.get_rules()
        for r in tree_rules:
            grammar.add_rule(r.start, r.end)

# Compute probabilities
grammar.calculate_probabilities()

# Optional Smoothing
grammar.do_smoothing()

# Optionally save grammar to file for reuse
grammar.save_to_file('/path/to/grammar_file')
```

# Entry Points

We will use BuildPCFG.py to extract a PCFG from a training corpus.
The result is a grammar file. Then, we load that file back into memory for the CKY parser.


Execute:

Step A - Build a PCFG

	python BuildPCFG.py trainingFile outputGrammarFile

Step B - Load PCFG from file, and parse test sentences using CKY

	python parse.py grammarFile testSentencesFile


------------------------------------------------------------

Requirements:
- Python 3.5


Files:

- BuildPCFG.py : the main script that reads and parses a treebank and writes the PCFG file
- cky.py : the class that implements the CKY algorithm
- config.py : some configuration values
- parse.py : the main script that reads a PCFG file and the test file and outputs the results
- pcfg.py : the class that implements the grammar structures
- tree.py : class used for converting a tree from string into an actual tree structure
