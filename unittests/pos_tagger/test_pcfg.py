import os
from unittest import TestCase

from nlp.pos_tagger import tree, util
from nlp.pos_tagger.pcfg import PCFG

TEST_TREEBANK_FILENAME = os.path.join(os.path.dirname(__file__), 'data', 'test.treebank')
TEST_PCFG_FILENAME = os.path.join(os.path.dirname(__file__), 'data', 'test.pcfg')


class TestPCFG(TestCase):
    def test_build_with_treebank_save_and_reload(self):
        pcfg = PCFG()
        tree_count = 0
        with open(TEST_TREEBANK_FILENAME, 'r') as treebank:
            for line in treebank:
                my_tree = tree.parse(line.strip())
                tree_rules = my_tree.get_rules()
                for r in tree_rules:
                    pcfg.add_rule(r.start, r.end)
                tree_count += 1
        self.assertEqual(tree_count, 3)

        if os.path.exists(TEST_PCFG_FILENAME):
            os.remove(TEST_PCFG_FILENAME)
        self.assertFalse(os.path.exists(TEST_PCFG_FILENAME))

        pcfg.calculate_probabilities()
        pcfg.save_to_file(TEST_PCFG_FILENAME)

        self.assertTrue(os.path.exists(TEST_PCFG_FILENAME))
        pcfg_reloaded = PCFG.from_file(TEST_PCFG_FILENAME, util.DummyLogger())

        self.assertSetEqual(pcfg_reloaded.terminals, {'cats', 'eat', 'crabs', 'with', 'claws', 'scratch', 'people', 'walls'})
        self.assertSetEqual(pcfg_reloaded.non_terminals, {'TOP', 'S', 'NP', 'N', 'VP', 'V', 'PP', 'P'})

    def test_build_with_treebank_unknowns(self):
        pcfg = PCFG()
        with open(TEST_TREEBANK_FILENAME, 'r') as treebank:
            for line in treebank:
                my_tree = tree.parse(line.strip())
                tree_rules = my_tree.get_rules()
                for r in tree_rules:
                    pcfg.add_rule(r.start, r.end, has_leaf=r.has_leaf)

        self.assertIn('xxxunknown_', pcfg.terminals)
