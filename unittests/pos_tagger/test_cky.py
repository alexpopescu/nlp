from unittest import TestCase

from nlp.pos_tagger import util, pcfg, cky
from nlp.pos_tagger.exceptions import InvalidTerminalError, NoParseForSentenceError


class TestCYK(TestCase):
    def setUp(self):
        self.grammar = pcfg.PCFG(util.DummyLogger())
        self.grammar.add_rule('TOP', ('S',))
        self.grammar.add_rule('S', ('A', 'B'))
        self.grammar.add_rule('A', ('C', 'D'))
        self.grammar.add_rule('A', ('C', 'F'))
        self.grammar.add_rule('B', ('c',))
        self.grammar.add_rule('B', ('E', 'B'))
        self.grammar.add_rule('C', ('a',))
        self.grammar.add_rule('D', ('b',))
        self.grammar.add_rule('E', ('c',))
        self.grammar.add_rule('F', ('A', 'D'))
        self.grammar.calculate_probabilities()

    def test_no_left_hand_side_for_terminal(self):
        cky_processor = cky.CKY(self.grammar, util.DummyLogger())
        with self.assertRaises(InvalidTerminalError):
            cky_processor.parse('abcd')

    def test_viterbi_parse_successful(self):
        cky_processor = cky.CKY(self.grammar, util.DummyLogger())
        cky_processor.parse('aaabbbcc')
        parse = cky_processor.get_viterbi_parse()
        self.assertTrue(parse)

    def test_viterbi_parse_not_found(self):
        cky_processor = cky.CKY(self.grammar, util.DummyLogger())
        cky_processor.parse('ababcbcc')
        with self.assertRaises(NoParseForSentenceError):
            cky_processor.get_viterbi_parse()
