from unittest import TestCase

from nlp.pos_tagger.smooth import Pair, GoodTuringFrequencyEstimator


class TestPair(TestCase):
    def test_create_pair(self):
        pair = Pair(2)
        self.assertEqual(pair.r, 2)
        self.assertEqual(pair.n, 1)
        self.assertEqual(pair.log_Z, 0)
        self.assertEqual(pair.p, 0)
        self.assertEqual(pair.r_star, 0)
        self.assertEqual(pair.Z, 0)
        self.assertNotEqual(str(pair), '')

    def test_increment(self):
        pair = Pair(2)
        self.assertEqual(pair.n, 1)
        pair.inc()
        self.assertEqual(pair.n, 2)
        pair.inc(3)
        self.assertEqual(pair.n, 5)
        pair.inc(-1)
        self.assertEqual(pair.n, 4)

    def test_calculations(self):
        pair = Pair(5, 10)
        self.assertEqual(pair.Z, 0)
        self.assertEqual(pair.log_Z, 0)
        pair.calculate_z(50)
        self.assertNotEqual(pair.Z, 0)
        self.assertNotEqual(pair.log_Z, 0)

    def test_smoothing(self):
        pair = Pair(4, 20)
        smooth = pair.get_smooth(0.7, 0.5)
        self.assertNotEqual(smooth, 0)


class TestGoodTuring(TestCase):
    def test_initialization(self):
        good_turing = GoodTuringFrequencyEstimator()
        self.assertListEqual(good_turing.pairs, [])
        self.assertEqual(good_turing.intercept, 0)
        self.assertEqual(good_turing.slope, 0)
        self.assertEqual(good_turing.pZero, 0)
        self.assertEqual(good_turing.total, 0)
        self.assertNotEqual(good_turing.confidence_factor, 0)

    def test_add_pair(self):
        good_turing = GoodTuringFrequencyEstimator()
        self.assertEqual(len(good_turing.pairs), 0)
        good_turing.add_pair(1, 4)
        self.assertEqual(len(good_turing.pairs), 1)
        good_turing.add_pair(2, 5)
        good_turing.add_pair(3, 3)
        self.assertEqual(len(good_turing.pairs), 3)
        good_turing.add_pair(3, 2)
        self.assertEqual(len(good_turing.pairs), 3)
        self.assertEqual(good_turing.pairs[2].n, 5)

    def test_find_pair(self):
        good_turing = GoodTuringFrequencyEstimator()
        self.assertEqual(good_turing.find_pair(1), -1)
        good_turing.add_pair(1, 4)
        good_turing.add_pair(2, 5)
        good_turing.add_pair(3, 3)
        self.assertEqual(good_turing.find_pair(1), 0)
        self.assertEqual(good_turing.find_pair(2), 1)
        self.assertEqual(good_turing.find_pair(3), 2)
        self.assertEqual(good_turing.find_pair(4), -1)

    def test_sort_pairs(self):
        good_turing = GoodTuringFrequencyEstimator()
        good_turing.add_pair(2, 5)
        good_turing.add_pair(3, 3)
        good_turing.add_pair(1, 4)
        good_turing.add_pair(4, 7)
        self.assertEqual(good_turing.find_pair(2), 0)
        self.assertEqual(good_turing.find_pair(3), 1)
        self.assertEqual(good_turing.find_pair(1), 2)
        self.assertEqual(good_turing.find_pair(4), 3)
        good_turing.sort_pairs()
        self.assertEqual(good_turing.find_pair(1), 0)
        self.assertEqual(good_turing.find_pair(2), 1)
        self.assertEqual(good_turing.find_pair(3), 2)
        self.assertEqual(good_turing.find_pair(4), 3)

    def test_find_best_fit(self):
        good_turing = GoodTuringFrequencyEstimator()
        good_turing.add_pair(2, 5)
        good_turing.add_pair(3, 3)
        good_turing.add_pair(1, 4)
        good_turing.add_pair(4, 7)
        self.assertEqual(good_turing.slope, 0.0)
        self.assertEqual(good_turing.intercept, 0.0)
        good_turing.calculate_pairs_z()
        good_turing.find_best_fit()
        self.assertNotEqual(good_turing.slope, 0.0)
        self.assertNotEqual(good_turing.intercept, 0.0)
