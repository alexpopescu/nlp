from unittest import TestCase

from nlp.pos_tagger import tree
from nlp.pos_tagger.exceptions import InvalidTreeError


class TestTree(TestCase):
    def __check_node(self, node, label, children_count=0):
        self.assertEqual(node.label, label)
        self.assertEqual(len(node.children), children_count)

    def test_parsing(self):
        my_tree = tree.parse('(TOP (S (NP John) (VP (V hit) (NP (DT the) (N ball)))))')
        self.__check_node(my_tree, 'TOP', 1)
        self.__check_node(my_tree[0], 'S', 2)
        self.__check_node(my_tree[0][0], 'NP', 1)
        self.__check_node(my_tree[0][1], 'VP', 2)
        self.__check_node(my_tree[0][0][0], 'John')
        self.__check_node(my_tree[0][1][0], 'V', 1)
        self.__check_node(my_tree[0][1][1], 'NP', 2)
        self.__check_node(my_tree[0][1][0][0], 'hit')
        self.__check_node(my_tree[0][1][1][0], 'DT', 1)
        self.__check_node(my_tree[0][1][1][1], 'N', 1)
        self.__check_node(my_tree[0][1][1][0][0], 'the')
        self.__check_node(my_tree[0][1][1][1][0], 'ball')

    def test_invalid_tree(self):
        with self.assertRaises(InvalidTreeError):
            tree.parse('(TOP (S (NP John) (VP (V hit) (NP (DT the) (N ball))))')
