import os
from datetime import datetime

import logging
from logging import handlers


class DummyLogger:
    def __getattr__(self, name):
        return lambda *x: None


def logify_name(name):
    return '.'.join(os.path.basename(name).split('.')[:-1])


def create_logger(script, path=None, level=None):
    script = logify_name(script)
    log = logging.getLogger(script.upper())
    if level is not None:
        log.setLevel(level)
    else:
        log.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create console handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_handler.setFormatter(formatter)
    log.addHandler(console_handler)

    # create file handler
    if path is not None:
        full_path = os.path.join(path, '{}_{:%Y%m%d_%H%M%S}.log'.format(script, datetime.now()))
        file_handler = logging.FileHandler(full_path)
        file_handler.setLevel(level)
        file_handler.setFormatter(formatter)
        log.addHandler(file_handler)

    return log


def create_mp_listener(queue, script, path=None, level=None):
    script = logify_name(script)
    log = logging.getLogger(script.upper())
    if level is not None:
        log.setLevel(level)
    else:
        log.setLevel(logging.DEBUG)

    handler_list = []

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create console handler
    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_handler.setFormatter(formatter)
    handler_list.append(console_handler)

    # create file handler
    if path is not None:
        full_path = os.path.join(path, '{}_{:%Y%m%d_%H%M%S}.log'.format(script, datetime.now()))
        file_handler = logging.FileHandler(full_path)
        file_handler.setLevel(level)
        file_handler.setFormatter(formatter)
        handler_list.append(file_handler)

    listener = handlers.QueueListener(queue, *handler_list)
    listener.start()
    return listener


def create_mp_logger(queue, name, level=None):
    log = logging.getLogger(name)
    if level is not None:
        log.setLevel(level)
    else:
        log.setLevel(logging.DEBUG)

    queue_handler = handlers.QueueHandler(queue)
    log.addHandler(queue_handler)

    return log
