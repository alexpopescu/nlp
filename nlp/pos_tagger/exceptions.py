class NlpError(Exception):
    """Base class for exceptions in this module."""
    pass


class InvalidTerminalError(NlpError):
    """Exception raised for a terminal that doesn't have a corresponding rule in the grammar

    Attributes:
        terminal -- the terminal that caused the error
        message -- explanation of the error
    """

    def __init__(self, terminal, message):
        self.terminal = terminal
        self.message = message


class NoParseForSentenceError(NlpError):
    """Raised when a sentence cannot be parsed.

    Attributes:
        sentence -- the sentence that caused the error
        message -- explanation of the error
    """

    def __init__(self, sentence, message):
        self.sentence = sentence
        self.message = message


class InvalidTreeError(NlpError):
    """Raised when a tree cannot be parsed.

    Attributes:
        string -- the string representation of the tree
        message -- explanation of the error
    """

    def __init__(self, string, message):
        self.string = string
        self.message = message
