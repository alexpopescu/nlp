from nlp.pos_tagger import unknowns, config
from .exceptions import InvalidTreeError


def parse(string):
    """
        Splits a string into chunks, including label and children
    :param string: The string to be parsed
    :return:
    """
    if string[0] == "(":
        if string[-1] != ")":
            raise InvalidTreeError(string, 'This tree cannot be parsed')
        string = string[1:-1]

    node = Node(string[:string.find(" ")])
    if string.find("(") == -1:
        node.add_child(label=string[string.find(" ") + 1:])
    else:
        while string.find("(") > -1:
            first_par = string.find("(")
            close_par = node.find_par(string, first_par + 1)
            try:
                child = parse(string[first_par:close_par])
            except InvalidTreeError:
                raise InvalidTreeError(string, 'This tree cannot be parsed')
            child.parent = node
            node.children.append(child)
            string = string[close_par + 1:]
    return node


class BaseRule:
    """
    Represents a basic rule extracted from a tree with the form: A -> [B C ...]
    """
    
    def __init__(self, start, end, has_leaf=False):
        """
            Constructor
        :param start: The start of the rule
        :param end: The end value set of this rule
        :param has_leaf:
        """
        self.start = start
        self.end = end
        self.has_leaf = has_leaf

    def __str__(self):
        return self.start + "->" + (" ".join("%s" % e for e in self.end))


class Node:
    """
    Represents one node in a parsed tree
    """

    # Constructor
    # @param string input - The string to be parsed
    # @param node parent  - The parent of this node (optional)
    def __init__(self, label=None, children=None, parent=None):
        """
            Constructor
        """
        self.label = label or ''
        self.children = children or []
        self.parent = parent
    
    def is_leaf(self):
        return len(self.children) == 0

    def has_leaf(self):
        return len(self.children) == 1 and self.children[0].is_leaf()

    def add_child(self, label):
        self.children.append(Node(label, parent=self))

    def __getitem__(self, index):
        return self.children[index]

    def __len__(self):
        return len(self.children)

    @staticmethod
    def find_par(string, pos):
        """
            Finds closing bracket
        :param string: The string to search in
        :param pos: The position where to start the search
        :return: The position of the closing bracket or -1 if not found
        """
        parcount = 1
        i = pos+1
        while parcount > 0 and i < len(string):
            if string[i] == "(":
                parcount += 1
            elif string[i] == ")":
                parcount -= 1
            i += 1
        if parcount == 0:
            return i
        else:
            return -1

    def get_rules(self):
        """
            Returns the basic rules of this tree. These include all relationships
            between nodes or between nodes and terminals
        :return: The array of basic rules
        """
        ret = []
        if self.is_leaf():
            return ret
        # if len(self.children) == 0 and self.terminal != None:
        #    ret.append(baseRule(label, [self.filterWord(self.terminal.lower())]))
        # else:
        ret.append(BaseRule(self.filter_label(), self.get_children_labels(), self.has_leaf()))

        for c in self.children:
            for r in c.get_rules():
                ret.append(r)
        return ret

    def get_children_labels(self):
        """
            Gets the labels of all children if any, or the terminal node
        :return: The labels
        """
        ret = []
        for c in self.children:
            ret.append(c.filter_label())
        return ret
    
    def filter_label(self):
        if self.is_leaf():
            if unknowns.is_feature_number(self.label):
                return unknowns.NUMBER_SUFFIX
        #   return self.label.lower()
        return self.label

    # TODO: check and remove
    # resolves all fake unaries and binarizations
    # def cleanUp2(self):
    #    for i,c in enumerate(self.children):
    #        # this node contains fake unaries
    #        if config.unariesMarker in c.label:
    #            splitLabel = c.label.split(config.unariesMarker, 1)
    #            c2 = node()
    #            c2.parent = self
    #            c2.label = splitLabel[0]
    #            self.children[i].label = splitLabel[1]
    #            c2.children.append(self.children[i])
    #            self.children[i] = c2
    #        # this node is a binarization result
    #        if c.label.endswith(config.binarizationSuffix):
    #            c.cleanUp() # since the newly inserted nodes are not in the initial binarization
    #            # they need to be cleaned before we copy them into the children set
    #            for i2,c2 in enumerate(c.children):
    #                c2.parent = self
    #                self.children.insert(i+i2+1, c2)
    #            del self.children[i]
    #        self.children[i].cleanUp()

    def clean_up(self):
        """
            resolves all fake unaries and binarizations
        :return:
        """
        self.toDelete = []
        # this node contains fake unaries
        if config.unariesMarker in self.label:
            split_label = self.label.split(config.unariesMarker, 1)
            self.label = split_label[0]
            c2 = Node()
            c2.parent = self
            c2.label = split_label[1]
            for i, c in enumerate(self.children):
                self.children[i].parent = c2
            c2.children = self.children[:]
            del self.children[:]
            self.children.append(c2)
            
            # FIX set parents right
            # for c in c2.children:
            #   c.parent = c2
              
        # this node is a binarization result
        if self.label.endswith(config.binarizationSuffix) and self.parent is not None:
            i = self.parent.children.index(self)
            i2 = 0
            while i2 < len(self.children):
                self.children[i2].parent = self.parent
                # print 'inserting own child %d (%s) to parent at %d' % (i2, self.children[i2].label, i+i2+1)
                self.parent.children.insert(i+i2+1, self.children[i2])
                i2 = i2 + 1
                
            del self.parent.children[i]
            self.parent.toDelete.append(i)
        else:
            i2 = 0
            while i2 < len(self.children):
                before = len(self.children)
                self.children[i2].clean_up()
                if before == len(self.children):
                    i2 = i2 + 1

    def to_string(self, indent=0):
        """
            Creates a printable form of the tree, including indentation based on level
        :param indent: The level of indentation (optional)
        :return: The string representation
        """
        s = "  " * indent + self.label + "\n"
        for c in self.children:
            s += c.to_string(indent + 1)
        return s

    def __str__(self):
        if self.is_leaf():
            return self.label
        return "(%s %s)" % (self.label, (" ".join("%s" % c for c in self.children)))
