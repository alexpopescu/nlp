import math

# ----------------------------------------------------------------------------
# This file contains structures needed for smoothing and the Good-Turing alg.
# Inspired from: http://www.grsampson.net/D_SGT.c
# ----------------------------------------------------------------------------


class Pair:
    """
    Represents a pair of values for the Good-Turing algorithm
    """

    def __init__(self, frequency, frequency_count=1):
        """
            Constructor
        :param frequency: the number of occurrences
        :param frequency_count: the number of times we have r occurrences
        """
        self.r = frequency
        self.n = frequency_count
        self.log_r = math.log(self.r)
        self.r_star = 0
        self.p = 0
        self.Z = 0
        self.log_Z = 0

    def inc(self, frequency_count=1):
        """
            Increments the n number by a given value
        :param frequency_count: The incrementing value (optional)
        :return:
        """
        self.n += frequency_count

    def calculate_z(self, num):
        """
            Calculates the Z value for this pair (and implicitly the log(Z) value)
        :param num: The total N sum of occurrences
        :return:
        """
        self.Z = 2 * self.n / num
        self.log_Z = math.log(self.Z)

    def get_smooth(self, intercept, slope):
        """
            Gets the smoothed version of this pair's probability
        :param intercept: The intercept value for this set (calculated below)
        :param slope: The slope value for this set (calculated below)
        :return: The smoothed probability
        """
        return (self.r + 1) * math.exp(intercept + slope * math.log(self.r + 1)) / math.exp(
            intercept + slope * self.log_r)

    def __str__(self):
        return "%s -> %s" % (self.r, self.p)


class GoodTuringFrequencyEstimator:
    """
    This class represents a smoothing "mechanism" which gets as input several pairs
    of values and then calculates the smoothing values of this set
    """

    def __init__(self):
        self.pairs = []
        self.total = 0
        self.confidence_factor = 1.96
        self.pZero = 0
        self.slope = 0
        self.intercept = 0

    def find_pair(self, _r):
        """
            Searches for the pair with a specific r-value
        :param _r: The value to look for
        :return: The position of the pair, or -1 if not found
        """
        for index, p in enumerate(self.pairs):
            if p.r == _r:
                return index
        return -1

    def add_pair(self, frequency, frequency_count=1):
        """
            Adds a pair if this r-value does not exist or increments the n-value if the pair is found in the set
        :param frequency: The r-value of the pair
        :param frequency_count: The n-value of the pair
        :return:
        """
        p = self.find_pair(frequency)
        if p == -1:
            self.pairs.append(Pair(frequency, frequency_count))
        else:
            self.pairs[p].inc(frequency_count)
        self.total += frequency * frequency_count

    def sort_pairs(self):
        """
            Sorts the pairs by r-values ascending
        """
        self.pairs.sort(key=lambda x: x.r)

    def calculate_pairs_z(self):
        num = sum(pair.n for pair in self.pairs)
        for pair in self.pairs:
            pair.calculate_z(num)

    def find_best_fit(self):
        """
            Calculates the values of the slope and interception which will be used for smoothing
        """
        xys = 0.0
        x_squares = 0.0
        mean_x = 0.0
        mean_y = 0.0

        for p in self.pairs:
            mean_x += p.log_r
            mean_y += p.log_Z
        mean_x /= len(self.pairs)
        mean_y /= len(self.pairs)
        for p in self.pairs:
            xys += (p.log_r - mean_x) * (p.log_Z - mean_y)
            x_squares += math.pow(p.log_r - mean_x, 2)
        if x_squares == 0:
            return
        self.slope = xys / x_squares
        self.intercept = mean_y - self.slope * mean_x

    def do_smoothing(self):
        """
            Runs the complete smoothing algorithm
        """
        if len(self.pairs) == 1:
            return

        next_n = self.find_pair(1)
        if next_n < 0:
            self.pZero = 0
        else:
            self.pZero = self.pairs[next_n].n / self.total

        for j, p in enumerate(self.pairs):
            i = 0 if j == 0 else self.pairs[j - 1].r
            if j == len(self.pairs) - 1:
                k = 2 * self.pairs[j].r - i
            else:
                k = self.pairs[j + 1].r
            p.calculate_z(k - i)

        self.find_best_fit()
        indiff_vals_seen = False
        for p in self.pairs:
            y = p.get_smooth(self.intercept, self.slope)
            next_index = self.find_pair(p.r + 1)
            if next_index < 0:
                indiff_vals_seen = True
            if not indiff_vals_seen:
                next_n = self.pairs[next_index].n
                x = (p.r + 1) * next_n / p.n
                # TODO: make more readable
                test_val = self.confidence_factor * math.sqrt(math.pow(p.r + 1, 2) *
                                                              next_n / math.pow(p.n, 2) *
                                                              (1 + next_n / p.n))
                if math.fabs(x - y) <= test_val:
                    indiff_vals_seen = True
                else:
                    p.r_star = x
            if indiff_vals_seen:
                p.r_star = y
        big_n_prime = sum([p.n * p.r_star for p in self.pairs])
        for p in self.pairs:
            p.p = (1 - self.pZero) * p.r_star / big_n_prime

    def get_probability(self, frequency):
        """
            Gets the probability for a specific r-value
        :param frequency: The r-value to search for
        :return: The probability (or 0 if not found)
        """
        i = self.find_pair(frequency)
        if i < 0:
            return 0
        return self.pairs[i].p

    def __str__(self):
        return "0 -> %s\n%s" % (self.pZero, "\n".join("%s" % p for p in self.pairs))
