from collections import defaultdict

from .exceptions import NoParseForSentenceError, InvalidTerminalError
from nlp.pos_tagger import tree, unknowns, config


class CKY:
    """
    Implementation of the CKY algorithm
    """

    def __init__(self, grammar, log):
        self.grammar = grammar
        self.log = log
        self.table = None

    # add an A --> B [ C .. ] item to the table (probabilistic)
    def add(self, i, k, j, rule_lhs, rule_rhs, prob):
        self.log.debug("CKY added: (%d, %d, %d) %s %f" % (i, k, j, rule_lhs, prob))
        self.table[i, j].add(rule_lhs)

        # store complete parse forest - project requirement
        if config.COMPLETE_PARSE_FOREST:
            self.backpoint[i, j, rule_lhs].add((k, rule_rhs, prob))

        # viterbi data (store the best backpointer and probability)
        if prob > self.pChart[i, j, rule_lhs]:  # returns 0.0 if nonexistent
            self.pChart[i, j, rule_lhs] = prob  # store best prob of this cell & rule_lhs
            self.viterbiBackpoint[i, j, rule_lhs] = (k, rule_rhs)

    def modify_tokens(self, sentence):
        """
            replace unknown words with features (possibly lowercase)
        :param sentence: the sentence to modify
        :return: a copy with the modifications
        """
        modified_sentence = []
        for i, word in enumerate(sentence):

            # get feature
            feature = unknowns.get_feature(word)

            # lowercase the word if configured
            if config.LOWERCASE_INPUT:
                word = word.lower()

            if len(self.grammar.backward_lookup[(word,)]) > 0:
                # word is known, use the original word
                modified_sentence.append(word)
            else:
                # use xxxunknown+feature instead of actual word
                modified_sentence.append(feature)

        return modified_sentence

    def parse(self, sentence):
        """
            to parse a sentence
        :param sentence:
        :return:
        """

        # replace unknown words with feature-standins
        # this results in modified_sentence
        # the actual sentence remains as it was!
        modified_sentence = self.modify_tokens(sentence)

        self.sentence = sentence
        # self.modifiedSentence = modified_sentence
        self.table = defaultdict(set)  # stores non-terminals
        self.pChart = defaultdict(float)  # stores probabilities
        self.backpoint = defaultdict(set)  # *all* backpointers for a (cell, nt)
        self.viterbiBackpoint = defaultdict(tuple)  # the best backpointer for a (cell, nt)

        n = len(sentence)

        # log that we are parsing this sentence
        self.log.info("Original sentence: %s", sentence)
        self.log.info("Modified sentence: %s", modified_sentence)

        # make some instance variables locally accessible
        chart = self.table
        pChart = self.pChart
        grammar = self.grammar
        backpoint = self.backpoint
        viterbiBackpoint = self.viterbiBackpoint

        # CKY main loop
        for j in range(n):

            word = modified_sentence[j]
            original_word = sentence[j]

            rhs = (word,)
            original_rhs = (original_word,)
            starts = self.grammar.backward_lookup[rhs]

            if len(starts) == 0:
                msg = "No left-hand side in the grammar for terminal: %s" % word
                self.log.error(msg)
                raise InvalidTerminalError(word, msg)

            for lhs in starts:
                prob = self.grammar.probability[lhs, rhs]
                self.add(j, -1, j, lhs, original_rhs, prob)
                # fix to deal with POS unaries, e.g. XP --> POS
                self.__add_unaries(j, (lhs,), prob)

            # fill in table based on grammar and already filled in word cells
            for i in range(j - 1, -1, -1):  # [j-1 .. 0]
                for k in range(i, j + 1):   # [  i .. j]

                    self.log.debug("CKY is at i,k,j  %d %d %d" % (i, k, j))

                    for rhs1 in chart[i, k]:
                        for rhs2 in chart[k + 1, j]:

                            rhs = (rhs1, rhs2)
                            lhss = self.grammar.backward_lookup[rhs]

                            for lhs in lhss:
                                p1 = self.grammar.probability[lhs, rhs]
                                p2 = pChart[i, k, rhs1]
                                p3 = pChart[k + 1, j, rhs2]
                                prob = p1 * p2 * p3

                                self.add(i, k, j, lhs, rhs, prob)

        # add unaries, but only of the form TOP --> XP
        # here we deal with cell i,j which is now complete
        covering = []
        for nt in chart[0, n - 1]:
            prob = self.grammar.probability['TOP', (nt,)]

            if prob > 0.0:
                new_prob = prob * pChart[0, n - 1, nt]
                covering.append([0, n - 1, 'TOP', (nt,), new_prob])

        for item in covering:
            i, j, lhs, rhs, prob = item
            self.add(i, -1, j, lhs, rhs, prob)

        return covering

    def __add_unaries(self, index, rhs, prob):
        for lhs in self.grammar.backward_lookup[rhs]:
            new_prob = prob * self.grammar.probability[lhs, rhs]
            self.add(index, -1, index, lhs, rhs, new_prob)

    def __str__(self):
        s = ""
        n = len(self.sentence)

        for i in range(n):

            # whitespace indentation
            for x in range(i):
                s += "\t"

            # print cells in this row
            for j in range(i, n):
                for x in self.table[i, j]:
                    s += "%s " % x
                s += "\t"
            s += "\n"

        return s

    def get_viterbi_parse(self):
        """
            Returns the Viterbi parse for this table
        :return:
        """

        n = len(self.sentence)
        i, j, root = 0, n - 1, 'TOP'

        # check if there is a parse to find (i.e. > 0)
        if len(self.viterbiBackpoint[i, j, root]) == 0:
            msg = "There is no parse for this sentence."
            self.log.error(msg)
            raise NoParseForSentenceError(self.sentence, msg)

        return self.__get_viterbi_parse(i, j, root)

    def __get_viterbi_parse(self, i, j, root):
        """
            Internal recursive method for Viterbi extraction
        :param i:
        :param j:
        :param root:
        :return:
        """

        # from the lhs, get the rhs using our backpointers
        # k is the splitting point
        # rhs is of the form (A,) or (A, B) or (terminal,)
        k, rhs = self.viterbiBackpoint[i, j, root]

        # create a subtree
        n = tree.Node()
        n.label = root

        # recursive step
        # case: unary or POS --> terminal
        if k == -1:
            if rhs[0] != self.sentence[i]:  # unary
                rhs1 = rhs[0]
                ch = self.__get_viterbi_parse(i, j, rhs1)
                ch.parent = n
                n.children.append(ch)

            else:  # terminal
                ch = tree.Node()
                ch.label = rhs[0]
                ch.parent = n
                n.children.append(ch)

        # case: A --> B C
        else:
            rhs1 = rhs[0]
            rhs2 = rhs[1]

            ch1 = self.__get_viterbi_parse(i, k, rhs1)
            ch2 = self.__get_viterbi_parse(k + 1, j, rhs2)

            ch1.parent = n
            ch2.parent = n

            n.children.append(ch1)
            n.children.append(ch2)

        # return the subtree from root
        return n


if __name__ == "__main__":
    pass
