# Contains all the word suffixes
import re

SUFFIXES = ["ed", "ing", "er", "est", "ize", "fy", "ly", "ble", "ness", "less", "ment", "ism", "ist", "al", "ish"]

# Prefix of unknown elements
UNKNOWN = "xxxunknown_"

# other suffixes used for unknown features
TITLE_SUFFIX = UNKNOWN + "title"
HYPHEN_SUFFIX = UNKNOWN + "hyphen"
NUMBER_SUFFIX = UNKNOWN + "number"

# the regular expression used to match numbers
NUMBERS_REGEX = "^[-+]?\d{1,3}(\d|[,]?\d{3})*(\.\d+|[eE][-+]?[0-9]+)?$"


def is_feature_number(word):
    """
        checks if the word is a number
    """
    if re.match(NUMBERS_REGEX, word) is not None:
        return True
    return False


def is_feature_title(word):
    """
        checks if the word is a title
    """
    return word.istitle()


def is_feature_hyphen(word):
    """
        checks if the word contains a hyphen
    """
    return word.find("-") != -1


def is_feature_suffix(word):
    """
        checks if the word has one of the defined suffixes
    :param word:
    :return: the suffix or None if not found
    """
    for suffix in SUFFIXES:
        if word.lower().endswith(suffix):
            return suffix
    return None


def get_feature(word):
    """
        returns the most appropriate feature for a word
    """
    if is_feature_number(word):
        return NUMBER_SUFFIX
    if is_feature_title(word):
        return TITLE_SUFFIX
    suffix = is_feature_suffix(word)
    if suffix is not None:
        return UNKNOWN + suffix
    if is_feature_hyphen(word):
        return HYPHEN_SUFFIX
    return UNKNOWN


def get_all_features(word):
    """
        returns all the matching features for a word
    """
    if is_feature_number(word):
        yield NUMBER_SUFFIX
    if is_feature_title(word):
        yield TITLE_SUFFIX
    suffix = is_feature_suffix(word)
    if suffix is not None:
        yield UNKNOWN + suffix
    if is_feature_hyphen(word):
        yield HYPHEN_SUFFIX
    yield UNKNOWN
