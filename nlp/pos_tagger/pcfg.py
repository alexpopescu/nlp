from collections import defaultdict

from .util import DummyLogger
from nlp.pos_tagger import smooth, unknowns


class PCFG:
    """
    Represents an entire grammar
    """

    def __init__(self, log=None):
        self.non_terminals = set()
        self.terminals = set()
        self.forward_lookup = defaultdict(set)
        self.backward_lookup = defaultdict(set)
        self.probability = defaultdict(float)
        self.counts = defaultdict(int)
        self.log = log or DummyLogger()

    @staticmethod
    def from_file(input_file, log):
        """
            Reads a PCFG grammar from a file
        :param input_file: the name (and full path) of the input file
        :param log: the logger object
        :return: a PCFG grammar
        """
        log.info('Loading grammar from file: %s', input_file)
        grammar = PCFG(log)

        got_non_terminals = False
        got_terminals = False
        rule_count = 0
        with open(input_file, 'r') as fin:
            for line in fin:
                if not got_non_terminals:
                    grammar.non_terminals = set(line.strip().split(' '))
                    log.info('Loaded {} non-terminal symbols'.format(len(grammar.non_terminals)))
                    got_non_terminals = True
                    continue
                if not got_terminals:
                    grammar.terminals = set(line.strip().split(' '))
                    log.info('Loaded {} terminal symbols'.format(len(grammar.terminals)))
                    got_terminals = True
                    continue
                parts = line.strip().split(' ')
                start = parts[0]
                prob = float(parts[1])
                end = parts[2:]
                grammar.add_rule_probability(start, end, prob)
                rule_count += 1
        log.info('Loaded {} rules'.format(rule_count))

        return grammar

    def save_to_file(self, output_file):
        self.log.info('Saving grammar to file: %s', output_file)

        with open(output_file, 'w') as f_out:
            f_out.write(self.__repr__())

    def add_rule(self, start, end, occurrences=1, has_leaf=False):
        """
            Adds a new ruleSet to the grammar if the ruleSet with this start doesn't exist
            or adds the end to the set if it does
        :param start: The start of the rule
        :param end: The ruleEnd to be added
        :param occurrences: The number of occurrences (optional)
        :param has_leaf:
        :return:
        """
        u_start = start.upper()
        self.non_terminals.add(u_start)
        if has_leaf:
            u_end = tuple([end[0].lower()])
            self.add_unknown_rules(u_start, end)
        else:
            u_end = tuple(end)
        for end_entry in u_end:
            self.terminals.add(end_entry)

        self.forward_lookup[u_start].add(u_end)
        self.backward_lookup[u_end].add(u_start)
        self.counts[u_start, u_end] += occurrences

    def add_rule_probability(self, start, end, probability):
        """
            Adds a new rule set with probability to the grammar
        :param start: The start of the rule
        :param end: The rule end to be added
        :param probability:
        :return:
        """
        u_start = start
        u_end = tuple(end)

        self.forward_lookup[u_start].add(u_end)
        self.backward_lookup[u_end].add(u_start)
        self.probability[u_start, u_end] = probability

    def add_unknown_rules(self, start, end):
        """
            Checks if this ruleEnd matches any unknown rules and adds them or just
            increments their occurences if they already exist
        :param start: The start of the rule
        :param end: The array of elements in the ruleEnd
        :return:
        """
        if len(end) > 1:
            return

        for feature in unknowns.get_all_features(end[0]):
            self.add_rule(start, [feature])

    def rule_count(self):
        """
            Gets the total number of ruleSets in the grammar
        :return:
        """
        return len(self.counts)

    def do_smoothing(self):
        """
            Runs the Good-Turing smoothing algorithm for each ruleSet in the grammar
        :return:
        """
        to_delete = defaultdict(set)
        for key, value in self.forward_lookup.items():
            gt = smooth.GoodTuringFrequencyEstimator()
            for rightHandSide in value:
                if rightHandSide[0] != unknowns.UNKNOWN:
                    gt.add_pair(self.counts[key, rightHandSide])
            gt.sort_pairs()
            gt.do_smoothing()
            for rightHandSide in value:
                if rightHandSide[0] == unknowns.UNKNOWN:
                    if gt.pZero == 0:
                        to_delete[key] = rightHandSide
                    else:
                        self.probability[key, rightHandSide] = gt.pZero
                else:
                    self.probability[key, rightHandSide] = gt.get_probability(self.counts[key, rightHandSide])
        for key, value in to_delete.items():
            self.forward_lookup[key].remove(value)
            self.backward_lookup[value].remove(key)
            del self.counts[key, value]
            del self.probability[key, value]

    def calculate_probabilities(self, keep_non_zero_values=True):
        self.terminals = self.terminals - self.non_terminals

        for leftHandSide, rightHandSet in self.forward_lookup.items():
            total_count = 0
            for rightHandSide in rightHandSet:
                total_count += self.counts[leftHandSide, rightHandSide]
            for rightHandSide in rightHandSet:
                if not keep_non_zero_values or self.probability[leftHandSide, rightHandSide] == 0:
                    self.probability[leftHandSide, rightHandSide] = \
                        self.counts[leftHandSide, rightHandSide] / total_count

    def __repr__(self):
        return '{}\n{}\n{}'.format(
            ' '.join(self.non_terminals),
            ' '.join(self.terminals),
            '\n'.join(
                '\n'.join('{} {:.10f} {}'.format(
                    leftHandSide,
                    self.probability[leftHandSide, rightHandSide],
                    ' '.join(rightHandSide)
                ) for rightHandSide in rightHandSet)
                for leftHandSide, rightHandSet in self.forward_lookup.items()
            )
        )

    def __str__(self):
        return self.__repr__()
