import logging

# some program wide configuration

# set to True to print debug information
debug = False

# logging level
# logging.INFO (default), logging.DEBUG
LOGGING_LEVEL = logging.INFO

# skip sentences longer than this
# integer > 0, default 15
MAX_SENTENCE_LENGTH = 4000

# lowercase input
# True (default) | False
LOWERCASE_INPUT = True

# print all covering non-terminals for a table/sentence
# True | False (default)
PRINT_COVERING = False

# Build a complete parse forest (True) or not (False)
# If False, only the backpointers useful for extracting
# the Viterbi parse are stored
COMPLETE_PARSE_FOREST = False

# Remove unaries and debinarize output (Viterbi) trees
# True | False
REMOVE_FAKE_UNARIES_AND_DEBINARIZE = True

# data constants
unariesMarker = "%%%%%"
binarizationSuffix = "@"

